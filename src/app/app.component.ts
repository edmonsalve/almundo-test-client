import { Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';

@Component({
  selector: 'almundo-hotels',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {}

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  {
    path: "home",
    component: HomeComponent
  }
]

export const Router = RouterModule.forRoot(routes);