export interface Hotel {
    id: number,
    name: string,
    stars: number,
    price: string,
    image: string
    amenities: string[]
}
