import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent, Router } from './app.component';
import { HomeComponent } from './home/home.component';
import { FiltersComponent } from './filters/filters.component';
import { HotelComponent } from './hotel/hotel.component';
import { HotelsService } from './services/hotels/hotels.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FiltersComponent,
    HotelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    Router,
  ],
  providers: [
    HotelsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
