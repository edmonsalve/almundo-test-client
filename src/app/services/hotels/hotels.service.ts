import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from './../../../environments/environment';

import { Hotel } from './../../models/hotel';

@Injectable()
export class HotelsService {

  public apiUrl = environment.apiUrl;
  public endpoint = this.apiUrl + "/hotels";

  constructor (
    private http: HttpClient
  ) { }

  public getAll() {
    return this.http.get<Hotel[]>(this.endpoint, {
      params: new HttpParams().set("name", "").set("stars", "")
    });
  }

  public getByFilters(name: string, stars: string) {
    return this.http.get<Hotel[]>(this.endpoint, {
      params: new HttpParams().set("name", name).set("stars", stars)
    });
  }

}
 