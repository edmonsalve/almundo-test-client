import { Component, OnInit, Input } from '@angular/core';

import { Hotel } from './../models/hotel';

@Component({
  selector: 'hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {

  @Input() data: Hotel;

  constructor () {}

  public ngOnInit() {
  }

}
