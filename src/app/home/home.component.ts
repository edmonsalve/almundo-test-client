import { Component, OnInit } from '@angular/core';

import { Hotel } from './../models/hotel';
import { HotelsService } from '../services/hotels/hotels.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public hotels: Hotel[];

  constructor (
    private hotelsService: HotelsService
  ) { }

  public ngOnInit() {
    this.hotelsService.getAll().subscribe((data) => {
      this.hotels = data;
    }, (error) => {
      console.error(error);
    })
  }

  public filterHotels(data) {
    console.log("received", data);
    this.hotelsService.getByFilters(data.name, data.stars).subscribe((response) => {
      this.hotels = response;
    }, (error) => {
      console.error(error);
    })
  }

}
