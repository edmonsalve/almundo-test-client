import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  public hotelName: string = "";
  public hotelStars: any = 0;
  @Output() filter = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public getFilteredHotels() {
    if (this.hotelStars == 0) this.hotelStars = "";
    this.filter.emit({ name: this.hotelName, stars: this.hotelStars});
  }

}
