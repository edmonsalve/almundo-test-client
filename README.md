# ALMUNDO Test Cliente

Esta aplicación fue desarrollada usando una versión estable de Angular 4. por esto es importante que tenga instalado en su computador el CLI, `npm install -g @angular/cli`

Para ejecutar la aplicación solo debe ejecutar el comando:

`npm install` y luego:

`ng serve`  Recuerde que esto tomará por defecto el entorno de desarrollo
`ng serve --prod --env=prod` ó fue añadido el comando `npm run start:prod` que lanzarán el entorno productivo

Para la optimización de los assets y lo demás me apoye en el Angular CLI.


Espero les guste :) !